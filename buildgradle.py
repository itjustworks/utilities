import os

project = raw_input("Insert the project name: ");
package = raw_input("Insert the package as reverse dns: ")

project = project.lower()
project = project.title()
project = project.replace(" ", "")

os.system("mkdir "+project)
os.chdir(project)
print "create the file build.gradle..."
file = open("./build.gradle", "w")
file.write("apply plugin: 'java'\n")
file.write("apply plugin: 'eclipse'\n\n");
file.write("repositories {\n\tjcenter()\n}\n\n")
file.write("dependencies {\n\ttestCompile 'junit:junit:4.12'\n\ttestCompile 'com.github.stefanbirkner:system-rules:1.16.0'\n}\n\n")
file.write("sourceSets {\n\tmain.java.srcDir \"src/main\"\n\ttest.java.srcDir \"src/test\"\n}\n\n")
file.close()

path = package.replace(".", "/")

print "create the folders..."
os.system("mkdir -p ./src/main/"+path)
os.system("mkdir -p ./src/test/"+path)

print "create the Testfile..."
testfile = open("./src/test/"+path+"/"+project+"Test.java", "w")
testfile.write("package "+package+";\n\n")
testfile.write("import static org.junit.Assert.*;\n")
testfile.write("import org.junit.Test;\n\n")
testfile.write("public class "+project+"Test {\n\n")
testfile.write("\t@Test\n")
testfile.write("\tpublic void testExample() {\n")
testfile.write("\t\tassertEquals(3, 3);\n")
testfile.write("\t}\n")
testfile.write("}")
testfile.close()

print "create the MainFile..."
mainfile = open("./src/main/"+path+"/"+project+".java", "w")
mainfile.write("package "+package+";\n\n")
mainfile.write("public class "+project+" {\n\n")
mainfile.write("}")
mainfile.close()

print "create the wrapper..."
os.system("gradle wrapper")

print "test the app and clean..."
os.system("gradle test")
os.system("gradle clean")

project = project.lower()

print "create the git repository and commit it..."
os.system("git init")
os.system("git add .")
os.system("git commit -m \"Initial commit\"")
user = raw_input("Enter your username from bitbucket: ");
os.system("curl -k --user "+user+" https://api.bitbucket.org/1.0/repositories --data name="+project+"  --data is_private=true")
os.system("git remote add origin https://therickys93@bitbucket.org/"+user+"/"+project+".git")
os.system("git push -u origin --all")
