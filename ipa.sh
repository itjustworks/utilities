#!/bin/bash

# remember to put the target as the first paramenter
rm *.ipa
# set new $IFS
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
FILES="$1/Info/*.plist"
for plist in $FILES
do
    echo "Updating $plist"

    if [[ -z "$CFBundleVersionNew" ]]
    then
    	CFBundleVersionString=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" $1.xcodeproj/../$1/Info.plist)
        CFBundleVersion=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" $1.xcodeproj/../$1/Info.plist)
         CFBundleVersionNew=$(($CFBundleVersion + 1))
    fi

    /usr/libexec/PlistBuddy -c "Set :CFBundleVersion $CFBundleVersionNew" "$1.xcodeproj/../$1/Info.plist"

    CFBuildDate=$(date)

    /usr/libexec/PlistBuddy -c "Set :CFBuildDate $CFBuildDate" "$1.xcodeproj/../$1/Info.plist"

    touch "$1.xcodeproj/../$1/Info.plist"
done
# restore $IFS
IFS=$SAVEIFS
xcodebuild clean -project $1.xcodeproj -configuration Release -alltargets
xcodebuild archive -project $1.xcodeproj -scheme $1 -archivePath $1.xcarchive
xcodebuild -exportArchive -archivePath $1.xcarchive -exportPath ./$1-v$CFBundleVersionString.$CFBundleVersionNew.ipa -exportFormat ipa
xcodebuild clean -project $1.xcodeproj -configuration Release -alltargets
rm -Rf *.xcarchive
rm -Rf build
