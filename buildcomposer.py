import os

project = raw_input("Insert the project name: ")
project = project.lower()
project = project.title()
project = project.replace(" ", "")

os.system("mkdir "+project)
os.chdir(project)

print "create the composer.json file..."
composer = open("./composer.json", "w")
composer.write("{\n\t\"require\": {\n\t\t\"phpunit/phpunit\":\"4.7.7\"\n\t},\n\t\"autoload\":{\n\t\t\"psr-0\": {\"\":\"src/\"}\n\t}\n}")
composer.close()

print "create the source directory..."
os.system("mkdir ./src");

print "create the test directory..."
os.system("mkdir ./test")

print "create the test file..."
testfile = open("./test/"+project+"Test.php", "w")
testfile.write("<?php\n\tclass "+project+"Test extends PHPUnit_Framework_TestCase\n\t{\n\t\tpublic function testExample() {\n\t\t\t$this->assertTrue(true);\n\t\t}\n\t}\n?>")
testfile.close()

print "creat the main file..."
mainfile = open("./src/"+project+".php", "w")
mainfile.write("<?php\n\tclass "+project+"\n\t{\n\t}\n?>")
mainfile.close()

print "run composer and test..."
os.system("composer install")
os.system("./vendor/bin/phpunit --colors=auto ./test")
os.system("rm -Rf vendor composer.lock");

project = project.lower()

print "create the git repository and commit it..."
os.system("git init")
os.system("git add .")
os.system("git commit -m \"Initial commit\"")
user = raw_input("Enter your username from bitbucket: ");
os.system("curl -k --user "+user+" https://api.bitbucket.org/1.0/repositories --data name="+project+"  --data is_private=true")
os.system("git remote add origin https://therickys93@bitbucket.org/"+user+"/"+project+".git")
os.system("git push -u origin --all")
